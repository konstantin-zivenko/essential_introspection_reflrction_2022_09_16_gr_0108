class Vehicle:
    def __init__(self, color, model):
        self.color = color
        self._model = model
        self._possible_models = ["Audi", "Hyundai", "Zaz", "Toyota"]

    def __str__(self):
        return f'Car {self.color} : {self.model} : {self.year}'


    def __eq__(self, other):
        return (self.year == other.year) and (self.model == other.model)


    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, model):
        if model in self._possible_models:
            self._model = model
        print("Incorrect model!")


class Device:
    def __init__(self):
        self._voltage = 12


class Car(Vehicle, Device):
    def __init__(self, color, model, year):
        Vehicle.__init__(self, color, model)
        Device.__init__(self)
        self.year = year

    @property
    def voltage(self):
        return self._voltage

    @voltage.setter
    def voltage(self, volts):
        print("Warning: this can cause problems!")
        self._voltage = volts

    @voltage.deleter
    def voltage(self):
        print("Warning: the radio will stop working!")
        del self._voltage



if __name__ == "__main__":
    print(__name__)
    import property_example
