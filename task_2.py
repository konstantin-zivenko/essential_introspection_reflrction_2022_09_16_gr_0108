# Створити клас Contact з полями surname, name, age, mob_phone, email. Додати методи get_contact,
# sent_message. Створити клас UpdateContact з полями surname, name, age, mob_phone, email, job. Додати
# методи get_message. Використати можливість роботи с атрибутами __dict__, __base__, __bases__.
# Роздрукувати інформацію на екрані.


class Contact:
    def __init__(self, name, surname, age, mob_phone, email):
        self.name = name
        self.surname = surname
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    def get_contact(self):
        return f" name: {self.name}\nsurname: {self.surname}\nmob_phone"