class Person:
    def __init__(self, year, name, gender):
        self.name = name
        self.gender = gender
        self.year = year

    def __str__(self):
        return f"I`m is {self.gender}, my name is {self.name}, ! was born in {self.year}"


print(__name__)